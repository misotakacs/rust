pub fn read_int(a: &[u8]) -> Vec<i32> {    
    let mut nums = Vec::new();
    let mut have = false;
    let mut num:i32 = 0;
    for i in 0..a.len() {
        let b = a[i] as i32;
        if b >= 0x30 && b <= 0x39 {
            have = true;
            num = num * 10;
            num = num + b - 0x30; 
        } else if have {
            nums.push(num);
            have = false;
            num = 0; 
        }
    }
    nums
}

#[cfg(test)]
mod tests {
    use super::read_int;

    #[test]
    fn it_works() {
        let a : [u8; 10] = [0x31, 0x32, 0x39, 0x20, 0x20, 0x33, 0x33, 0x30, 0x30, 0x0];
        let vec = read_int(&a);
        assert_eq!(2, vec.len());
        assert_eq!(129, vec[0]);
        assert_eq!(3300, vec[1]);
    }
}

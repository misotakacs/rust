extern crate rustc_serialize as serialize;

use std::io::{self,BufRead};
use serialize::hex::{FromHex,ToHex};

fn main() {
    let mut line1 = String::new();
    let mut line2 = String::new();
    let stdin = io::stdin();
    stdin.lock().read_line(&mut line1).unwrap();
    stdin.lock().read_line(&mut line2).unwrap();
    let vec1: Vec<u8> = line1.from_hex().unwrap();
    let vec2: Vec<u8> = line2.from_hex().unwrap();
    if vec1.len() != vec2.len() {
        panic!("The strings are of different length!");
    }
    let mut vec3 = Vec::new();
    for (ix, u) in vec1.iter().enumerate() {
        let mut v = vec2[ix];
        v = u ^ v;
        vec3.push(v);        
    }
    println!("Xored: {}", vec3.to_hex());
}

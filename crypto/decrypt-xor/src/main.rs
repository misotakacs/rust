/**

1. Generate English frequency analysis dictionary.
The output is a map of scoring:

[letter] -> [position]

e.g.

e -> 1
a -> 2
...
x -> 26

The value expresses the absolute position in frequency.

2. Analyze the ciphertext, generate a similar map.

-----

MEH just try all combinations and see when something is just ASCII.

*/

extern crate rustc_serialize as serialize;

use std::cmp::Ordering::Less;
use std::collections::HashMap;
use std::io::{self,BufRead};
use serialize::hex::FromHex;

fn decrypt(ct: &Vec<u8>, ot: &mut Vec<u8>, key:u8) {
    for c in ct {
        ot.push(c ^ key);
    }
}

fn score(ot: &Vec<u8>) -> f32 {
    let mut score: f32 = 0.0;
    let mut space_score: f32 = 0.0;
    for u in ot {
        if *u > 0x20 && *u < 0x7d {
            score += 1.0;
        }
        if *u == 0x20 {
            space_score += 2.0;
        }
    }
    (score + space_score) / ot.len() as f32 
}

fn main() {
    let mut ciphertext = String::new();  
    let stdin = io::stdin();
    stdin.lock().read_line(&mut ciphertext).unwrap();
    let ct = ciphertext.from_hex().unwrap();
    let mut results = HashMap::new();
    let mut r;
    for key in 0..255 {        
        r = Vec::new();
        decrypt(&ct, &mut r, key);
        let s = score(&r);
        results.insert(r, s);
    }
    let mut res_vec: Vec<(&Vec<u8>, &f32)> = results.iter().collect();
    res_vec.sort_by(|a, b| (*b.1).partial_cmp(a.1).unwrap_or(Less));
    let mut ix = 0;
    for (res, sc) in res_vec {    
        let r = res.clone();
        println!("Score: {}: {}", sc, String::from_utf8(r).unwrap());
        ix += 1;
        if ix > 20 {
            break;
        }
    }
}

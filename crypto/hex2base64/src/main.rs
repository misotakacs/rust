extern crate rustc_serialize as serialize;

use std::io::prelude::*;
use std::fs::File;
use serialize::hex::FromHex;

fn to_base64(v: Vec<u8>) -> String {
    let a = br"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
    let mut b = Vec::new();
    let mut ix = 0;
    let mut bits = 0;
    let mut c1;
    for u in v {        
        if ix == 0 {
            // Take top 6 bits.
            c1 = u >> 2;
            // Preserve bottom 2 bits.
            bits = (u & 3) << 4;
        } else if ix == 1 {
            // Use preserved 2 bits and add top 4 bits.
            c1 = bits | ((u & 0xF0) >> 4);
            // Preserve bottom 4 bits.
            bits = (u & 0xF) << 2;
        } else {
            // Use preserved 4 bits and add top 2 bits.
            c1 = bits | ((u & 0xC0) >> 6);
            b.push(a[c1 as usize]);
            // Use bottom 6 bits. 
            c1 = u & 0x3F;
        }
        b.push(a[c1 as usize]);
        ix = (ix + 1) % 3;
    }
    if ix > 0 {
        // TODO(miso): Need to pad/
    }
    String::from_utf8(b).unwrap()
}

fn main() {
  let mut f = File::open("data/input.txt").ok().expect("Cannot open file");
  let mut s = String::new();
  f.read_to_string(&mut s).ok().expect("Failed to read string");

  let s2: &str = &s;  
  let vec: Vec<u8> = s2.from_hex().unwrap();

  let s3 = to_base64(vec); 
  
  println!("{}", s3);
}
